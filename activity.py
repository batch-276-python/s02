# Description: S02 Activity

# 1. Accept a year input from the user and determine if it is a leap year or not.
# Add a validation for the leap year input:
# Strings are not allowed for inputs
# No zero or negative values are allowed for inputs

while True:
    try:
        year = int(input("Please enter a year: "))
        if year <= 0:
            print("Invalid input. Year must be greater than zero.")
            continue
        break
    except ValueError:
        print("Invalid input. Please enter a valid integer year.")

isLeapYear = year % 4 == 0 and year % 100 != 0 or year % 400 == 0

if isLeapYear:
    print(f"{year} is a leap year!")
else:
    print(f"{year} is not a leap year!")


# 2. Accept two numbers (row and col) from the user and create a grid of asterisks using the two numbers (row and col).

row = int(input("Please enter the number of rows: \n"))
col = int(input("Please enter the number of columns: \n"))

for i in range(row):
    for j in range(col):
        print("*", end=" ")
    print()





