# Python also allows user to input, with this, users can give input to the program

# [Section] Input
# username = input("Please enter your name: \n")
#
# print(f"Hello {username}! Welcome to the world of Python")
#
# num1 = int(input("Enter first number: "))
# num2 = int(input("Enter second number: "))
# print(f"The sum of num1 and num2 is {num1 + num2}")

# [Section] With user inputs, users can give inputs for the program to be used to control the application using
# control Structures/Flow Control
# control Structures can be divided into selection and repetition control structures

# Selection control structures allows the program to choose among choices and run specific codes depending
# on the choice taken(conditional statements)

# Repetition control structures allows the program to repeat certain blocks of code given a starting condition
# and terminating condition

# [Section] If-else statements
# if-else statements are selection control structures that allows the program to choose between two or more choices

test_num = 75

if test_num >= 60:
    print("Test passed")
else:
    print("Test failed")

# Note that in Python, curly braces ({}) are not needed to define the code block inside the if or else statement.
# Hence, indentation are important as python uses indentation to define code blocks

# [Section] If else chains can also be used to have more than two choices for the program

# test_num2 = int(input("Please enter the second number \n"))
#
# if test_num2 > 0:
#     print("The number is positive!")
#     print(f"The number is {test_num2}")
# elif test_num2 == 0:
#     print("The number is equal to 0!")
#     print(f"The number is {test_num2}")
# else:
#     print("The number is negative!")
#     print(f"The number is {test_num2}")


# Mini Activity:

# number = int(input("Please enter a number: \n"))
#
# if number % 3 == 0 and number % 5 == 0:
#     print("The number is divisible by 3 and 5")
# elif number % 3 == 0:
#     print("The number is divisible by 3")
# elif number % 5 == 0:
#     print("The number is divisible by 5")
# else:
#     print("The number is not divisible by 3 or 5")


# [Section] Loops
# Python has loops that can repeat blocks of code given a starting condition and terminating condition
# While loops are used to execute aa set of statements as long as a condition is true
# i = 0
#
# while i <= 5:
#     print(f"Current Value of i is {i}")
#     i += 1

# [Section] for loops are used for iteration over a sequence (list, tuple, string) or other iterable objects

# fruits = ["apple", "banana", "cherry"]  # list
#
# for individual_fruit in fruits:
#     print(individual_fruit)

# [Section] range() method
# to use the for loop to iterate through values, the range method can be used
# by default, the range method starts from 0 and increments by 1

# for x in range(6):
#     print(f"Current value of x is {x}!")

# range() method can also be used to specify the starting and ending values
# the range method can also be used to specify the increment value using the third parameter
# for x in range(6, 20, 3):
#     print(f"Current value of x is {x}!")

# [Section] break statement
# the break statement can be used to stop the loop before it has looped through all the items

# j = 4
# while j < 6:
#     print(f"Current value of j is {j}")
#     if j == 3:
#         break
#     j += 1

# [Section] continue statement
# the continue statement can be used to stop the current iteration of the loop, and continue with the next

k = 1
while k < 6:
    k += 1
    if k == 3:
        continue
    print(k)


